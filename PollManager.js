module.exports = class PollManager {
  constructor(n) {
    this.n = n;
    this.db = n.db;
    this.log = n.log;

    this.bot = n.bot;
    this.pollMessages = [];
  }

  init() {
    this.log.info(`init PollManager`);
  }

  async create(message, match) {
    const title = match[1].trim();

    const choicesRaw = match[2].split("|");
    if(choicesRaw.length > 20) {
      await this.bot.createMessage(message.channel.id, `❌ Nombre maximum de choix dépassé`);
      return;
    }

    const choices = [];
    let choicesText = "";
    let choice;
    for(let i = 0; i<choicesRaw.length; i++) {
      choice = {
        id: i,
        name: choicesRaw[i].trim(),
        emoji: String.fromCodePoint(0x1F1E6+i),
        count: 0
      }
      if(choice.name == "") continue;
      choices.push(choice);
      choicesText += `${choice.emoji} ${choice.name}\n`;
    }

    choicesText += "\n";

    if(choices.length < 2) {
      await this.bot.createMessage(message.channel.id, `❌ Minimum de 2 choix`);
      return;
    }

    const pollMessage = await this.bot.createMessage(message.channel.id, {
      content: `\:bar_chart: **${title}**`,
      color: 0xFF0000,
      embed: {
        description: choicesText,
        footer: { icon_url: "https://bdegeii.y3n.co/s/mstile-310x310.png", text: `BotDE ${this.n.version}` }
      }
    });

    this.pollMessages.push(pollMessage);
  
    for(let i = 0; i<choices.length; i++) {
      pollMessage.addReaction(choices[i].emoji);
    }
  }

  choice(message, emoji, userID) {

  }
}