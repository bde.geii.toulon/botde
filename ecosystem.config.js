module.exports = {
  apps: [
    {
      name: "bde.bot",
      script: "./index.js",
      watch: true,
      env: {
        "NODE_ENV": "production"
      }
    }
  ]
}