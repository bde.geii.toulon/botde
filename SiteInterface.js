const { ifError } = require('assert');
const WebSocket = require('ws');

module.exports = class SiteInterface {
  constructor(n) {
    this.n = n;
    this.log = n.log;

    this.active = false;
    this.retries = 0;
  }

  init() {
    this.ws = new WebSocket('ws://127.0.0.1:8095/');
    if(this.pingTimeout) clearTimeout(this.pingTimeout);

    const heartbeat = () => {
      clearTimeout(this.pingTimeout);
      this.pingTimeout = setTimeout(() => {
        if(this.active == false) {
          return;
        }
        this.active = false;
        this.ws.terminate();
        this.log.verbose("connection to website lost (no ping)");
        this.reconnect();
      }, 5000 + 1000);
    }

    this.ws.on('open', () => {
      this.active = true;
      this.retries = 0;
      this.send({ type: "botauth", code: this.n.config.creds.wsCode });
      heartbeat();
    });

    this.ws.on('ping', () => {
      heartbeat();
    });

    this.ws.on('message', (data) => {
      this.handle(data);
    });

    this.ws.on('error', (error) => {
    });

    this.ws.on('close', (data) => {
      this.active = false;
      if(this.retries == 0) this.log.verbose("connection to website lost (closed)");
      this.reconnect();
    });
  }

  handle(message) {
    message = this.decode(message);
    if (message == null) return;

    if (message.type == "authok") {
      this.log.info("connected and authenticated to website");
      return;
    }

    if (message.type == "verification") {
      this.n.verification(message.data);
      return;
    }
  }

  send(message) {
    if(!this.active || this.ws.readyState !== WebSocket.OPEN) return;
    if(!message.type) throw "No type on message";
    this.ws.send(JSON.stringify(message));
  }

  reconnect() {
    this.retries++;
    setTimeout(async () => {
      this.init();
    }, 5000);
  }

  decode(message) {
    try {
      return JSON.parse(message);
    } catch (e) {
      return null;
    }
  }
}