const BotDE = require("./BotDE");
const config = require("./config");

(async () => {
  const bot = new BotDE(config);
  bot.init();
})();