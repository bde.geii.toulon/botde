const https = require("https");
const ical = require('node-ical');

module.exports = class EDTInterface {
  constructor(config, userConfig) {
    this.version = "1.0.0";
    this.config = config;
    this.userConfig = userConfig;
    this.cache = {};
  }

  async get() {
    if(new Date() - this.cacheDate < this.config.cacheTTL) return this.cache;
    
    return await this.reload();
  }

  async getCurrents() {
    let courses = await this.get();
    let n = new Date();
    return courses.filter((course) => {
      return course.start < n && course.end > n
    });
  }

  async getNext(s, d, de) {
    let courses = await this.get();
    let n = d || new Date();
    courses = courses.filter((course) => {
      return course.end > n
    });

    if(de) {
      courses = courses.filter((course) => {
        return course.end < de
      });
    }
    
    courses.sort((a, b) => {
      return a.start.getTime()-b.start.getTime();
    });

    return courses.slice(0, s || 10);
  }

  async reload() {
    let data = await this.downloadSource();
    let events = await this.parseSource(data);
    let courses = await this.formatSource(events);
    this.cacheDate = new Date();
    this.cache = courses;
    return courses;
  }

  downloadSource() {
    return new Promise((r) => {
      https.get(this.userConfig.icalLink, {}, (res) => {
        if(res.statusCode !== 200) {
          console.log('statusCode:', res.statusCode);
          console.log('headers:', res.headers);
          console.error("an error occured during source download");
        }

        let buf = "";
        res.on('data', (d) => {
          buf += d;
        });

        res.on('end', () => {
          r(buf);
        });
      })
    });
    
  }

  async parseSource(data) {
    const events = await ical.async.parseICS(data);
    
    return events;
  }

  async formatSource(events) {
    let courses = [];
    let entries = Object.entries(events);
    for(let [key, value] of entries) {
      if(key.startsWith("Ferie-")) continue;
      if(key.startsWith("ANNULE-")) continue;

      courses.push(value);
    }
    return courses;
  }
}