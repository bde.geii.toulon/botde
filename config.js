module.exports = {
  dev: process.env.NODE_ENV == "production" ? false : true,
  log: {
    info: true,
    warn: true,
    verbose: true,
    prefix: "BotDE"
  },
  creds: require("./creds"),
  db: {
    name: "bde"
  },
  discordIDs: {
    guildID: "751752935607762994",
    verificationChannelID: "754695298043084951",
    welcomeChannelID: "758970092699910174",
    birthdaysChannelID: "780212365546881056",
    announcementChannelID: "751758165711781918"
  },
  devDiscordIDs: {
    guildID: "617845779931070484",
    verificationChannelID: "754698366239375422",
    welcomeChannelID: "758970170382876672",
    birthdaysChannelID: "754698366239375422",
    announcementChannelID: "617850154007003137"
  }
}