module.exports = class Birthdays {
  constructor(n, config) {
    this.n = n;

    this.bot = n.bot;
    this.db = n.db;
    this.log = n.log;
  }

  async init() {
    await this.loadTomorrow();
  }

  async loadTomorrow() {
    const birthdayMembers = await this.getFromDB();

    const tomorrowMidnight = new Date();
    tomorrowMidnight.setDate(tomorrowMidnight.getDate() + 1);
    tomorrowMidnight.setHours(0, 0, 0, 0);

    let timeToGo = tomorrowMidnight - new Date();

    // debug
    //const timeToGo = 2000;

    this.log.info(`[bd] got ${birthdayMembers.length} birthday(s) for tomorrow`);
    if (birthdayMembers.length !== 0) this.log.info(`[bd] sending in ${timeToGo} ms`);

    this.nextTimeout = setTimeout(async () => {
      if (birthdayMembers.length !== 0) await this.sendNotification(birthdayMembers);
      this.loadTomorrow();
    }, timeToGo);
  }

  async getFromDB() {
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    const dateExpr = { date: tomorrow, timezone: "Europe/Paris" };

    return await this.db.collection("members").find({
      "$expr": {
        "$and": [
          { "$eq": [{ "$dayOfMonth": "$dateNaissance" }, { "$dayOfMonth": dateExpr}] },
          { "$eq": [{ "$month": "$dateNaissance" }, { "$month": dateExpr }] }
        ]
      }
    }).toArray();
  }

  async sendNotification(members) {
    const text = this.formatBirthdays(members);
    const message = await this.bot.createMessage(this.n.discordIDs.birthdaysChannelID, {
      content: text
    });
    message.addReaction("🎉");
  }

  formatBirthdays(members) {
    let text = "";
    let age;
    for (let member of members) {
      age = this.getAge(member.dateNaissance);
      if (age < 2) continue;
      text += `🎂 Joyeux anniversaire à <@${member.discordID}> ! Profite bien de tes ${age} ans ! 🎉\n`
    }

    return text;
  }

  getAge(birthDate) {
    const today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
}
