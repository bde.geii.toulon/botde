const Eris = require("eris");
const https = require("https");

const Logger = require("./Logger");
const Database = require("./Database");
const SiteInterface = require("./SiteInterface");
const PollManager = require("./PollManager");
const Birthdays = require("./Birthdays");

module.exports = class BotDE {
  constructor(config) {
    this.version = "4.0.0";
    this.config = config;
    this.c = config;

    this.discordIDs = this.c.dev ? this.config.devDiscordIDs : this.config.discordIDs
    this.startDate = new Date();
  }

  async init() {
    this.log = new Logger(this.c);
    this.log.info(`starting BotDE${this.c.dev ? ".dev" : ""} ${this.version}`);
    this.dbManager = new Database(this);
    this.db = await this.dbManager.dbConnect();

    this.site = new SiteInterface(this);
    this.site.init();

    await this.startBot();
    

    this.log.info(`BotDE ${this.version} ready`);
  }

  async startBot() {
    this.bot = new Eris(this.c.dev ? this.config.creds.discordDevToken : this.config.creds.discordToken);

    this.pollManager = new PollManager(this);
    await this.pollManager.init();

    this.birthdays = new Birthdays(this);
    this.birthdays.init();

    this.bot.on("messageCreate", (message) => {
      if(message.content.startsWith("!")) {
        this.handleCommand(message);
      }
    });

    this.bot.on("error", async (e) => {
      //console.error(e);
      await this.recordError(e);
    });

    this.bot.on("messageReactionAdd", async (message, emoji, userID) => {
      if(this.pollManager.pollMessages.find(pollMessage => pollMessage.id == message.id)) {
        this.pollManager.choice(message, emoji, userID);
        return;
      }

      // verification
      if(message.locked) return;
      message.locked = true;

      if(userID !== this.c.creds.discordClientID && message.channel.id == this.discordIDs.verificationChannelID) {
        if(["✅", "❌"].indexOf(emoji.name) !== -1) {
          await this.handleVerificationCommand(message.id, userID, emoji.name == "✅");
        }
      }
      message.locked = false;
    });

    return new Promise((r) => {
      this.bot.on("ready", () => {
        r();
      });

      this.bot.connect();
    })
  }

  handleCommand(message) {
    this.log.verbose(`command ${message.content}`);

    if(message.content === "!stats") {
      this.handleStats(message);
      return;
    }

    if(message.content === "!pif") {
      this.handlePick(message);
      return;
    }

    if(message.content === "!vpif") {
      this.handleVoicePick(message);
      return;
    }

    if(message.content === "!info") {
      this.handleInfo(message);
      return;
    }

    const pollMatch = message.content.match(/^!sondage(.+?)?\|(.+)$/);
    if(pollMatch !== null) {
      this.pollManager.create(message, pollMatch);
      return;
    }

    const announceMatch = message.content.match(/^!annonce(.+)$/);
    if(announceMatch !== null) {
      this.handleAnnounce(message, announceMatch);
      return;
    }
  }

  async handleAnnounce(message, announceMatch) {
    const messageText = announceMatch[1].trim();
    if(messageText.length == 0) {
      await this.bot.createMessage(message.channel.id, "❌ Message d'annonce invalide");
      return;
    }

    await this.bot.createMessage(this.discordIDs.announcementChannelID, {
      content: `\📢 **Annonce du Staff BDE GEII** @everyone`,
      color: 0xFFFFFF,
      embed: {
        description: `**${messageText}**`,
        footer: { icon_url: "https://bdegeii.y3n.co/s/mstile-310x310.png", text: `BotDE ${this.version}` }
      }
    });
  }

  async handleStats(message) {
    const stats = await this.getStats();

    const reply = await this.bot.createMessage(message.channel.id, {
      embed: {
        title: `BotDE stats`,
        fields: [
          { name: "1FTP", value: stats["1FTP"] },
          { name: "1APP", value: stats["1APP"] },
          { name: "2FTP", value: stats["2FTP"] },
          { name: "2APP", value: stats["2APP"] },
          { name: "Total", value: stats.total }
        ]
      }
    });
  }

  async handlePick(message) {
    const member = await this.db.collection("members").aggregate([
      { $match: { active: true } },
      { $sample: { size: 1 } }
    ]).next();
    
    this.sendPickReply(message, member);
  }

  async handleVoicePick(message) {
    if(!message.member || !message.member.voiceState) return;
    if(typeof message.member.voiceState.channelID !== "string") {
      await this.bot.createMessage(message.channel.id, "❌ Tu dois être dans un salon vocal pour utiliser cette commande");
      return;
    }

    const voiceChannelID = message.member.voiceState.channelID;
    const guild = this.bot.guilds.get(this.discordIDs.guildID);
    const voiceChannel = guild.channels.get(voiceChannelID);
    const voiceMembersIDs = [...voiceChannel.voiceMembers.keys()];

    const member = await this.db.collection("members").aggregate([
      { $match: { active: true, discordID: { $in: voiceMembersIDs } } },
      { $sample: { size: 1 } }
    ]).next();

    if(member !== null) this.sendPickReply(message, member);
  }

  async sendPickReply(message, member) {
    let text;
    if(member.genre == 1) {
      text = `L'heureuse élue est <@${member.discordID}> !`;
    } else {
      text = `L'heureux élu est <@${member.discordID}> !`;
    }

    await this.bot.createMessage(message.channel.id, text);
  }

  async handleInfo(message) {
    const reply = await this.bot.createMessage(message.channel.id, {
      color: 0xFF0000,
      embed: {
        title: `BotDE BDE GEII`,
        description: `Bot développé par <@409383831758700554>\n\:timer: ${Math.floor((new Date() - this.startDate)/1000)} s`,
        footer: { icon_url: "https://bdegeii.y3n.co/s/mstile-310x310.png", text: `BotDE ${this.version}` }
      }
    });
  }

  async getStats() {
    const col = await this.db.collection("members");
    const stats = {
      "1FTP": await col.countDocuments({ annee: 1, groupe: { $ne: "APP" } }),
      "1APP": await col.countDocuments({ annee: 1, groupe: "APP" }),
      "2FTP": await col.countDocuments({ annee: 2, groupe: { $ne: "APP" } }),
      "2APP": await col.countDocuments({ annee: 2, groupe: "APP" })
    }

    stats.total = stats["1FTP"] + stats["1APP"] + stats["2FTP"] + stats["2APP"];

    return stats;
  }

  async verification(membre) {
    const msg = await this.bot.createMessage(this.discordIDs.verificationChannelID, {
      embed: {
        title: `Vérification : ${membre.prenom} ${membre.nom}`,
        fields: [
          { name: "Année", value: membre.annee.toString(), inline: true },
          { name: "Groupe", value: membre.groupe, inline: true },
          { name: "Email étudiant", value: membre.etudEmail, inline: true },
          { name: "Email", value: membre.email, inline: true },
          { name: "Date naissance", value: membre.dateNaissance.split("T")[0], inline: true }
        ]
      }
    });
    this.db.collection("members").updateOne({ id: membre.id }, { $set: { verificationMessageID: msg.id }});
    msg.addReaction("✅");
    msg.addReaction("❌");
  }

  async handleVerificationCommand(messageID, userID, isVerified) {
    const member = await this.db.collection("members").findOne({ verificationMessageID: messageID });
    if(!member) return;
    if(!member.awaitingVerification) return;

    await this.db.collection("members").updateOne({ id: member.id }, { $set: {
      active: isVerified,
      awaitingVerification: false,
      verifiedBy: userID
    }});

    if(isVerified) {
      const [statusCode, body] = await this.addMemberToGuild(member);

      if(statusCode == 201) {
        this.bot.createMessage(this.discordIDs.verificationChannelID, `\`${member.prenom} ${member.nom}\` vérifié avec succès par <@${userID}>`);
        this.bot.createMessage(this.discordIDs.welcomeChannelID, `Bienvenue parmis nous <@${member.discordID}>`);
      } else if(statusCode == 204) {
        this.bot.createMessage(this.discordIDs.verificationChannelID, `\`${member.prenom} ${member.nom}\` est déjà présent sur le serveur`);
      } else this.log.verbose(`invite ${statusCode} ${JSON.stringify(body)}`);
    } else {
      this.bot.createMessage(this.discordIDs.verificationChannelID, `\`${member.prenom} ${member.nom}\` rejeté par <@${userID}>`);
    }
  }

  async addMemberToGuild(member) {
    const reqBody = {
      access_token: await this.getMemberToken(member.id),
      nick: `${member.prenom} ${member.nom}`,
      roles: []
    }

    const verifiedRole = this.getRoleByName(`Vérifié`).id;
    const groupeRole = this.getRoleByName(`${member.annee}${member.groupe == "APP" ? "APP" : "FTP"}`).id;
    if(verifiedRole) reqBody.roles.push(verifiedRole);
    if(groupeRole) reqBody.roles.push(groupeRole);

    const [statusCode, body] = await this.req(`https://discord.com/api/guilds/${this.discordIDs.guildID}/members/${member.discordID}`, {
      method: "PUT",
      headers: {
        authorization: `Bot ${this.config.creds.discordToken}`,
        "content-type": "application/json"
      }
    }, reqBody);

    return [statusCode, body];
  }

  getRoleByName(name) {
    return this.bot.guilds.get(this.discordIDs.guildID).roles.find((a) => a.name == name);
  }

  async getMemberToken(memberID) {
    return (await this.db.collection("discordTokens").findOne({ memberID: memberID })).access_token;
  }

  req(url, options, data) {
    return new Promise((r) => {
      const req = https.request(url, options, (res) => {
        res.setEncoding('utf8');

        let body = "";
        res.on('data', (chunk) => {
          body += chunk;
        });
        res.on('end', () => {
          r([res.statusCode, body])
        });
      });

      req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
      });

      if (data) req.write(JSON.stringify(data));
      req.end();
    });
  }

  async recordError(e) {
    await this.db.collection("bot.errors").insertOne({
      version: this.version,
      date: new Date(),
      uptime: process.uptime(),
      startDate: this.startDate,
      e: e
    });
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}